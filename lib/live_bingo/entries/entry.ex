defmodule LiveBingo.Entries.Entry do
  use Ecto.Schema
  import Ecto.Changeset

  schema "entries" do
    field :phrase, :string
    field :points, :integer
    field :marked, :boolean, virtual: true, default: false

    timestamps()
  end

  @doc false
  def changeset(entry, attrs) do
    entry
    |> cast(attrs, [:phrase, :points])
    |> validate_required([:phrase, :points])
  end
end
