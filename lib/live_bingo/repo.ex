defmodule LiveBingo.Repo do
  use Ecto.Repo,
    otp_app: :live_bingo,
    adapter: Ecto.Adapters.Postgres
end
