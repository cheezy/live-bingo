defmodule LiveBingoWeb.PageLive do
  use LiveBingoWeb, :live_view

  alias LiveBingoWeb.BingoView
  alias LiveBingo.Entries
  alias LiveBingo.Scores

  def mount(_params, _session, socket) do
    socket =
      assign(
        socket,
        name: "Anonymous",
        game_number: 1,
        entering_game: true,
        name_input: "",
        score: 0,
        entries: Entries.list_entries(5)
      )
    {:ok, socket}
  end

  def render(assigns) do
    Phoenix.View.render(BingoView, "bingo_live.html", assigns)
  end

  def handle_event("save-name", %{"name_input" => name_input}, socket) do
    socket = socket
      |> clear_flash()
      |> assign(name: name_input, entering_game: false)
    {:noreply, socket}
  end

  def handle_event("cancel-name", _, socket) do
    socket = socket
      |> clear_flash()
      |> assign(name: socket.assigns.name, entering_game: false)
    {:noreply, socket}
  end

  def handle_event("marked", %{"id" => id}, socket) do
    entries = socket.assigns.entries
              |> mark_entry_with_id(String.to_integer(id))
    score = calculate_score(entries)
    socket = socket
      |> clear_flash()
      |> assign(entries: entries, score: score)
    {:noreply, socket}
  end

  def handle_event("new-game", _value, socket) do
    game_number = socket.assigns.game_number + 1
    socket = socket
      |> clear_flash()
      |> assign(
        game_number: game_number,
        score: 0,
        entries: Entries.list_entries(5)
      )
    {:noreply, socket}
  end
  
  def handle_event("share-score", %{"name" => name, "score" => score}, socket) do
    Scores.create_score(%{name: name, score: score})
    socket =
      socket
      |> put_flash(:info, "Your score of #{score} was successfully shared!")
    {:noreply, socket}
  end

  defp mark_entry_with_id(entries, id) do
    Enum.map(
      entries,
      fn entry ->
        case entry.id == id do
          true -> %{entry | marked: !entry.marked}
          _ -> entry
        end
      end
    )
  end

  defp calculate_score(entries) do
    Enum.reduce(entries, 0, fn x, acc -> accumulate_score(x, acc) end)
  end

  defp accumulate_score(en = %{marked: true}, acc), do: en.points + acc
  defp accumulate_score(_, acc), do: acc
end
