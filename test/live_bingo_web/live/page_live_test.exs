defmodule LiveBingoWeb.PageLiveTest do
  use LiveBingoWeb.ConnCase, async: true

  import Phoenix.LiveViewTest
  alias LiveBingoWeb.PageLive
  alias LiveBingo.Entries

  defp create_entry(phrase, points) do
    %{phrase: phrase, points: points}
    |> Entries.create_entry()
  end

  test "disconnected and connected render", %{conn: conn} do
    {:ok, page_live, disconnected_html} = live(conn, "/")
    assert disconnected_html =~ "BUZWORD BINGO"
    assert render(page_live) =~ "BUZWORD BINGO"
  end

  describe "Page header and who's playing" do
    test "initially shows Anonymous as the user", %{conn: conn} do
      {:ok, view, _html} = live(conn, "/")
      assert render(view) =~ "Anonymous"
    end

    test "initializes the game number to 1", %{conn: conn} do
      {:ok, view, _html} = live(conn, "/")
      assert render(view) =~ "Game #1"
    end

    test "only displays name entry when entering game" do
      params = %{
        name: "Anonymous",
        game_number: 1,
        entering_game: false,
        name_input: "",
        score: 0,
        entries: []
      }
      refute render_component(PageLive, params) =~ ".name-input"
    end

    test "updates the name of the user when provided", %{conn: conn} do
      {:ok, view, _html} = live(conn, "/")
      assert view
           |> element("#save_name")
           |> render_submit(%{name_input: "New User"})
         =~ "New User - Game #1"
    end

    test "hides the name entry when user cancels entry", %{conn: conn} do
      {:ok, view, _html} = live(conn, "/")
      view
        |> element("#cancel_name")
        |> render_click()

      refute has_element?(view, ".name-input")
    end
  end

  describe "Entries on the page" do
    test "are marked when they are selected", %{conn: conn} do
      create_entry("Entry One", 100)
      create_entry("Entry Two", 200)
      {:ok, view, _html} = live(conn, "/")

      view
        |> element("#entries li:first-child()")
        |> render_click()

      assert view
          |> element("#entries li:first-child()")
          |> render()
        =~ "class=\"marked\""

      refute view
          |> element("#entries li:nth-child(2)")
          |> render
        =~ "class\"marked\""
    end

    test "score is accumulated from selected entries", %{conn: conn} do
      create_entry("Entry One", 100)
      create_entry("Entry Two", 100)
      {:ok, view, _html} = live(conn, "/")

      view
        |> element("#entries li:nth-child(2)")
        |> render_click()

      assert render(view) =~ "<span class=\"value\">100</span>"
    end  end

  describe "Page actions" do
    test "New game increases the game number", %{conn: conn} do
      {:ok, view, _html} = live(conn, "/")

      assert render(view) =~ "Game #1"
      assert view
          |> element("#new_game")
          |> render_click()
        =~ "Game #2"
    end

    test "New game resets the game score", %{conn: conn} do
      create_entry("Some Phrase", 100)
      {:ok, view, _html} = live(conn, "/")

      assert render(view) =~ "<span class=\"value\">0</span>"
      
      assert view
          |> element("#entries li:first-child()")
          |> render_click()
        =~ "<span class=\"value\">100</span>"

      assert view
          |> element("#new_game")
          |> render_click()
        =~ "<span class=\"value\">0</span>"
    end

    test "Share score writes a notice", %{conn: conn} do
      create_entry("Some Phrase", 200)
      {:ok, view, _html} = live(conn, "/")

      view
        |> element("#entries li:first-child()")
        |> render_click()

      assert view
          |> element("#share_score")
          |> render_click()
        =~ "Your score of 200 was successfully shared!"
    end
  end
end
