# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     LiveBingo.Repo.insert!(%LiveBingo.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias LiveBingo.Repo
alias LiveBingo.Entries.Entry

%Entry{
  phrase: "Future-Proof",
  points: 100
}
|> Repo.insert!()

%Entry{
  phrase: "Doing Agile",
  points: 200
}
|> Repo.insert!()

%Entry{
  phrase: "In The Cloud",
  points: 300
}
|> Repo.insert!()

%Entry{
  phrase: "Rock-Star Ninja",
  points: 400
}
|> Repo.insert!()

%Entry{
  phrase: "Best of Breed",
  points: 150
}
|> Repo.insert!()

%Entry{
  phrase: "Reactive",
  points: 250
}
|> Repo.insert!()

%Entry{
  phrase: "Zero to Hero",
  points: 350
}
|> Repo.insert!()

%Entry{
  phrase: "User-Centric",
  points: 175
}
|> Repo.insert!()

%Entry{
  phrase: "Cross-Platform",
  points: 225
}
|> Repo.insert!()

%Entry{
  phrase: "Synergize",
  points: 375
}
|> Repo.insert!()
